package androidstudio_lab2.com.android.studio.temperatureapp;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 4/24/2019.
 */

public class Temperature {

    @SerializedName("feeds")
    public List<TempEntry> feeds;

    public List<TempEntry> getFeeds() {
        return feeds;
    }
}
