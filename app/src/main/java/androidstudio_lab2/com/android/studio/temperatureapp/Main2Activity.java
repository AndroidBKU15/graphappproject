package androidstudio_lab2.com.android.studio.temperatureapp;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,AndroidAsyncTask.ThingSpeakListener {

    private Handler handler;
    private Context mContext;
    private AndroidAsyncTask.ThingSpeakListener mListener;
    private LineChart lineChart;
    private List<Entry> entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lineChart = findViewById(R.id.linechart);
        entries = new ArrayList<>();
        entries.add(new Entry(1,35));
        entries.add(new Entry(2,34));
        entries.add(new Entry(3,38));
        entries.add(new Entry(4,40));
        entries.add(new Entry(5,25));

        LineDataSet dataSet = new LineDataSet(entries,"");
        dataSet.setColor(R.color.colorLineOfChart);
        dataSet.setLineWidth(3);
        dataSet.setValueTextSize(15);
        ValueFormatter valueFormatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return new DecimalFormat("#").format(value);
            }
        };
        dataSet.setValueFormatter(valueFormatter);


        LineData lineData = new LineData(dataSet);
        lineChart.setData(lineData);
        lineChart.invalidate();
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getXAxis().setAxisMinimum(0.9f);
        lineChart.getXAxis().setEnabled(false);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setTextSize(15);
        lineChart.getAxisLeft().setGranularity(1);
        lineChart.getAxisLeft().setAxisMinimum(20);

        Description description =  new Description();
        description.setText("");
        lineChart.setDescription(description);


        lineChart.getLegend().setEnabled(false);

        mContext = this;
        mListener = this;
        handler = new Handler();
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                new AndroidAsyncTask(mContext,mListener)
                        .execute("https://api.thingspeak.com/channels/728963/feeds.json?api_key=SDPPJKQST2D4MASS&results=1");
                handler.postDelayed(this, 2000);
            }
        };
        handler.postDelayed(r, 2000);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStreamResponse(String temperature) {
        entries.remove(0);
        for (Entry entry : entries){
            entry.setX(entry.getX()-1);
        }
        entries.add(new Entry(5,Float.parseFloat(temperature)));
        lineChart.notifyDataSetChanged();
        lineChart.invalidate();
    }

    @Override
    public void onStreamError(String message) {
        Log.d("Error",message);
    }
}
