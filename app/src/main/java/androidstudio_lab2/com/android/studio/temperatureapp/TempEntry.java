package androidstudio_lab2.com.android.studio.temperatureapp;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 4/24/2019.
 */

public class TempEntry {

    @SerializedName("field1")
    private String field1;

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

}
