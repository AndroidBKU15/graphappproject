package androidstudio_lab2.com.android.studio.temperatureapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import android.content.Context;


import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
//import com.loopj.android.http.HttpGet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by Le Trong Nhan on 20/02/2019.
 */

public class AndroidAsyncTask extends AsyncTask<String, List<TempEntry>, String> {
    private Context mContext;
    private ThingSpeakListener mListener;
    private static final String TAG = "GetFromThingSpeak";

    // constructor
    public AndroidAsyncTask(Context context, ThingSpeakListener listener) {
        mContext = context;
        mListener = listener;
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }



    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(String... f_url) {
        String url = f_url[0];
        Log.i(TAG, "URL - " + url);
        try {
            URL myURL = new URL(url);
            HttpsURLConnection urlConnection = (HttpsURLConnection) myURL.openConnection();

            InputStream instream = urlConnection.getInputStream();
            String response = convertStreamToString(instream);
            Log.i(TAG, "Response - " + response);

            Temperature obj = new Gson().fromJson(response, Temperature.class);
            List<TempEntry> temp = obj.getFeeds();
            Log.d(TAG, temp.get(0).getField1());
            publishProgress(temp);
            return "success";
        }catch(Exception e){
            Log.d(TAG, "Error - " + e);
        }

        return "can't connect";
    }

    @Override
    protected void onProgressUpdate(List<TempEntry>... temperature) {
        if (mListener == null) { return; }

        if(temperature[0].size()>0) {
            mListener.onStreamResponse(temperature[0].get(0).getField1());
        }else {
            mListener.onStreamError("Ko co ket qua tra ve");
        }
    }

    protected void onPostExecute(String temperature) {
        super.onPostExecute(temperature);
    }

    public interface ThingSpeakListener {
        void onStreamResponse(String temp);
        void onStreamError(String message);
    }

}

